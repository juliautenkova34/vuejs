export default {
    GET_TRANSLATIONS (state, translations) {
        state.translations = translations;
    },

    GET_SITE_MOBILE_TITLE (state, payload) {
        state.siteMobileTitle = payload;
    }
};