import Vue from 'vue';

export default {
    translations: state => {
        return state.translations;
    }
}