import Vue from 'vue';

export default {
    getTraslations (context, payload) {
        Vue.http.get('/data/website-translations.json').then(function(response) {
            context.commit('GET_TRANSLATIONS', response.data[payload])
        });
    },

    postContactForm (context, payload) {

        console.log( payload )

        return Vue.http.post('/form/lv').then(function(response) {
            console.log( response )
        });
    },

    getSiteMobileTitle (context, payload) {
        context.commit('GET_SITE_MOBILE_TITLE', payload)
    }
}