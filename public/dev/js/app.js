import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';

import { store } from './store/store';

import Header from './Header.vue';
import noiseComponent from './components/noise.component.vue';
import App from './App.vue';

import Contacts from './Contacts.vue';
import Support from './Support.vue';

import Home from './Home.vue';
import HomePost from './HomePost.vue';

import Blog from './Blog.vue';
import BlogPost from './BlogPost.vue';

import Team from './Team.vue';
import TeamMember from './TeamMember.vue';

Vue.use(VueRouter);
Vue.use(VueResource);

const routes = [
    { path: '/:lang/blog', name: 'blog', component: Blog },
    { path: '/:lang/blog/:postID', name: 'blogPost', component: BlogPost },

    { path: '/:lang/contacts', name: 'contacts', component: Contacts },
    { path: '/:lang/support', name: 'support', component: Support },

    { path: '/:lang/team', name: 'team', component: Team },
    { path: '/:lang/team/:memberId', name: 'teamMember', component: TeamMember },

    { path: '/:lang/:id', name: 'home', component: Home },
    { path: '/:lang/:id/post', name: 'homePost', component: HomePost },

    { path: '*', redirect: '/lv/01' },
];

const router = new VueRouter({
    routes: routes
});

Vue.component('app-header', Header);
Vue.component('noise-component', noiseComponent);

const vm = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});