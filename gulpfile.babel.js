// Basic gulp usage:
// npm install <package-name> --save-dev (install as dev dependency)
// OR
// npm install <package-name> --save (install as nescessary dependency)

/*******************************************************************************
 0. INSTALLATION STEPS
 *******************************************************************************/

// 1. Make sure you have all required software installed:
//   1.1. Node.js (https://nodejs.org/en/)
//   1.2. Gulp.js (npm install -g gulp)
//   1.3. Gulp.js in project (npm install --save-dev gulp)

// 2. Since we have bower.json, package.json and gulpfile.js files ready in project:
//   2.1. run npm install (this will install all dependencies in project root in node_modules folder)
//   2.2. run bower install (this will install all bower components in /web/bower_components folder)

// 3. Use "gulp" command for development environment:
//   3.1. It will compile all .scss files, concat .js files and will create nice folder /lib for bower components
//   3.2. IMPORTANT: I would love to place all working .js files into /web/assets/dev/js/includes folder and then
//        concat them into 1 working .js file

// 4. Use "gulp dist" for production tasks:
//   4.1 Compile .scss files into 1 .css file which will be minified and ready to use
//   4.2. Minify .js files

// IMPORTANT: DO NOT REMOVE .bowerrc file - it just changes the destination for bower_components folder

'use strict';

/*******************************************************************************
 1. DEPENDENCIES
 *******************************************************************************/
var Promise         = require('es6-promise').Promise;
var postcss 		= require('gulp-postcss');
var gulp 			= require('gulp');
var filter 			= require('gulp-filter');
var sass 			= require('gulp-sass');
var sourcemaps 		= require('gulp-sourcemaps');
var mainBowerFiles 	= require('main-bower-files');
var concat 			= require('gulp-concat');
var uglify 			= require('gulp-uglify');
var rename 			= require("gulp-rename");
var cssnano 		= require('gulp-cssnano');
var autoprefixer 	= require('gulp-autoprefixer');
var webpack         = require('webpack-stream');
var del             = require('del');
var babel           = require('gulp-babel');

var streamqueue  = require('streamqueue');

/*******************************************************************************
 2. FILE DESTINATIONS (RELATIVE TO ASSSETS FOLDER)
 *******************************************************************************/

// Define array with paths
// Just looks nice and clean
var main_dev_path = './public/dev';
var main_dist_path = './public/dist';
var path = {
    dev: {
        scss: main_dev_path + '/scss/**/*.scss',
        css: main_dev_path + "/css",
        js: main_dev_path + "/js"
    },
    dist: {
        css: main_dist_path + "/css",
        js: main_dist_path + "/js"
    }
};

/*******************************************************************************
 3. DEVELOPMENT TASKS
 *******************************************************************************/

gulp.task('clear-files', function () {
    return del([
        path.dist.js + '/**/*.js',
        path.dist.css + '/**/*.css'
    ]);
});

gulp.task('dev-scripts', function() {
    return gulp.src( path.dev.js + '/app.js' )
        .pipe(webpack( require('./webpack.config.js') ))
        .pipe(gulp.dest( path.dist.js ));
});

// Styles/CSS/SASS etc...
var processors = {
    browsers: ['last 15 versions']
};

// Styles/CSS/SASS etc...
gulp.task('dev-styles', function () {
    return gulp
        .src( path.dev.scss )
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer(processors))
        .pipe(concat('main.post.css'))
        .pipe(cssnano())
        .pipe(gulp.dest( path.dist.css ));
});

gulp.task('bower-js', function() {
    return streamqueue({ objectMode: true },
        gulp.src( [
            './public/assets/dev/js/internal-plugins/bootstrap-modal-vert-centerer.js'
        ] )
    )
        .pipe(concat('js-packages.main.js'))
        .pipe(uglify())
        .pipe(gulp.dest( path.dist.js ))
});

gulp.task('bower-css', function() {
    return streamqueue({ objectMode: true },
        gulp.src( [
            // './bower_components/owl.carousel/dist/assets/owl.carousel.css'
        ] )
    )
        .pipe(concat('css-packages.main.css'))
        .pipe(cssnano())
        .pipe(gulp.dest( path.dist.css ))
});

// Managing Bower files
gulp.task("bower-js-files", function(){

    var jsFilter = filter('**/*.js', {restore: true});

    gulp.src(mainBowerFiles())
        .pipe(jsFilter)
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest(path.dist.js));
});

gulp.task("bower-sass-files", function(){

    var scssFilter = filter('**/*.scss', {restore: true});

    gulp.src(mainBowerFiles())
        .pipe(scssFilter)
        .pipe(concat('vendor.scss'))
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('./sourcemaps'))
        .pipe(gulp.dest( path.dist.css ))
});

// What runs when we run "gulp" command
gulp.task('default', ['dev-styles', 'dev-scripts'], function() {
    gulp.watch(path.dev.scss, function() {
        gulp.run('dev-styles');
    });
    gulp.watch(path.dev.js + '/**/*.js', function() {
        gulp.run('dev-scripts');
    });
    gulp.watch(path.dev.js + '/**/*.vue', function() {
        gulp.run('dev-scripts');
    });
});

/*******************************************************************************
 3. PRODUCTION TASKS
 THIS ONE WE RUN WITH gulp dist in CLI
 *******************************************************************************/

gulp.task('dist', ['clear-files', 'dev-styles', 'dev-scripts']);


// 1. Compile SASS
// 2. Concatinate Bower js packages/libraries
// 3. Image optimization
// 4. Minify Js
// 5. Minify CSS